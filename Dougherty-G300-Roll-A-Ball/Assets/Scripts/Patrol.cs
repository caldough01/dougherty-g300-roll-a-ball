using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public Transform[] waypoints;
    private int currentWaypointIndex = 0;
    private float speed = 30F;

    public Rigidbody Ghost;
    public GameObject loseTextObject;
    public Timer time;

    void Update()
    {
        Transform wp = waypoints[currentWaypointIndex];
        if (Vector3.Distance(transform.position, wp.position) < 0.01f)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % waypoints.Length;
        }
        else
        {
            transform.position = Vector3.MoveTowards(
                transform.position,
                wp.position,
                speed * Time.deltaTime);
        }

        // Got code stuff from https://medium.com/geekculture/how-to-make-a-basic-patrolling-system-in-unity-c-3bec0cf63478

    }
}
