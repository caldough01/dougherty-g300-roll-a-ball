using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject loseTextObject;

    public GameObject instructions;

    private Rigidbody rb;
    private Rigidbody door;
    public Rigidbody Ghost;
    private int count;
    private float movementX;
    private float movementY;

    public Timer time;

    public float groundDistance = 0.5f;
    public Transform groundCheck;
    public LayerMask groundMask;
    bool isGrounded;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        door = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);

        instructions.SetActive(true);

    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (time.timeValue <= 115f)
        {
            instructions.SetActive(false);
        }
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void OnJump(InputValue movementValue)
    {
        if(isGrounded == true)
        {
            rb.AddForce(Vector3.up * 5f, ForceMode.Impulse);
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

            SetCountText();
        }

        if(other.gameObject.CompareTag("Enemy"))
        {

            time.timerRunning = false;
            

            loseTextObject.SetActive(true);

        }

        if (other.gameObject.CompareTag("Finish") && (count >= 8) && (time.timeValue > 0))
        {
            winTextObject.SetActive(true);

            time.timerRunning = false;
        }

        if (other.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        } 
        else
        {
            isGrounded = false;
        }
    }
}